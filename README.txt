
README.txt for ODT Importer 1.0.x

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Limitations
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Support/Customizations
 * Maintainers


INTRODUCTION
------------
ODT Importer is a module for Drupal that imports ODT files directly into drupal 
nodes (articles, pages...) or any custom entity. It produces HTML code from ODT 
files and places it to text field specified in configuration.

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/odt_importer


FEATURES
--------

Currently, the module can convert the following:

 * bold (<strong> tag)
 * italic (<em> tag)
 * underline (<u> tag)
 * quotations (<blockquote> tag)
 * images (with image upload feature)
 * links
 * headings (h1-h6)
 * lists (ul and li)
 * tables (table tr and td) with colspan and rowspan support
 * annotations
 * footnote
 * space character


LIMITATIONS
-----------

Everything that is not mentioned in the feature section is not supported.


REQUIREMENTS
------------

This module requires XMLReader and Zip PHP extensions, that are usually 
installed by default with php 5+.


INSTALLATION
------------

 * Install the ODT Importer module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Create a field type ".odt file importer" in desired node type or custom 
       entity.
    2. Choose appropriate text field for "Destination field" to be populated
       with converted HTML code.


TROUBLESHOOTING
---------------

If you have any problems feel free to create an issue 
https://www.drupal.org/node/add/project-issue/odt_importer


SUPPORT/CUSTOMIZATIONS
----------------------

Support by volunteers is available on:

 * https://www.drupal.org/project/issues/odt_importer?version=All&status=All

Please consider helping others as a way to give something back to the community
that provides Drupal and the contributed modules to you free of charge.

For paid support and customizations of this module, help with implementing an
ODT Importer module, or other Drupal work, contact the maintainer through his
contact form:

 * https://www.drupal.org/u/dillix


MAINTAINERS
-----------

 * Mikhail Khasaya (dillix) - https://www.drupal.org/u/dillix

Supporting organizations:

 * Dillix Media -
   https://dillix.com
