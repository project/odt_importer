<?php

namespace Drupal\odt_importer;

use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Global ToDos.
 *
 * 1. Parameter and return typehinting for class methods:
 * https://www.drupal.org/docs/develop/standards/coding-standards#s-parameter-and-return-typehinting.
 *
 * 2. Create templates for elements for easy overriding markup in themes.
 */

/**
 * Defines an ODT to HTML conversion implementation.
 */
class OdtConverter {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Features enabled for odt converter.
   *
   * @var array
   */
  protected $features;

  /**
   * Folder for storing images from odt file.
   *
   * @var string
   */
  protected $imagesFolder;

  /**
   * Converted html.
   *
   * @var array
   */
  protected $html;

  /**
   * Rendered footnotes.
   *
   * @var array
   */
  protected $footnotes;

  /**
   * Absolute path to converted .odt file.
   *
   * @var string
   */
  protected $odtFileAbsolutePath;

  /**
   * Initialize configuration values and run conversion.
   *
   * @param array $config
   *   An associative array of config options.
   * @param string $odt_file
   *   Path to parsed odt file.
   */
  public function __construct(array $config, $odt_file) {
    // Configuration settings.
    // @todo Add this settings to Drupal configuration GUI.
    $this->features['header'] = (bool) $config['features']['header'] ?? TRUE;
    $this->features['list'] = (bool) $config['features']['list'] ?? TRUE;
    $this->features['table'] = (bool) $config['features']['table'] ?? TRUE;
    $this->features['link'] = (bool) $config['features']['link'] ?? TRUE;
    $this->features['frame'] = (bool) $config['features']['frame'] ?? TRUE;
    $this->features['image'] = (bool) $config['features']['image'] ?? TRUE;
    $this->features['note'] = (bool) $config['features']['note'] ?? TRUE;
    $this->features['annotation'] = (bool) $config['features']['annotation'] ?? TRUE;
    $this->features['toc'] = (bool) $config['features']['toc'] ?? TRUE;
    $this->features['bookmark'] = (bool) $config['features']['bookmark'] ?? TRUE;

    $this->imagesFolder = $config['images_folder'] ?? 'public://images';

    $this->html = '';
    $this->footnotes = '';

    $this->odtFileAbsolutePath = \Drupal::service('file_system')->realpath($odt_file);

    $this->prepare();
  }

  /**
   * Function returns generated HTML markup and footnotes.
   *
   * @return string
   *   Generated HTML code with footnotes.
   */
  public function markup() {
    return $this->html . $this->footnotes;
  }

  /**
   * Function checks that the passed file is an image.
   *
   * @param string $file
   *   Path to image file.
   *
   * @return bool
   *   True, if passed file is image, otherwise false.
   */
  protected function isImage($file) {

    $image_extensions = ['jpg', 'jpeg', 'png', 'gif', 'svg', 'webp'];
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    if (!in_array($ext, $image_extensions)) {
      return FALSE;
    }

    // @todo Change mime_content_type to symphony MimeTypeGuesserInterface?
    return (strpos(@mime_content_type($file), 'image') === 0);
  }

  /**
   * Function makes file copy from ODT to Drupal file system.
   *
   * @param string $from
   *   Path to source.
   * @param string $to
   *   Path do destination.
   *
   * @return bool
   *   True, if passed file successfully copied, otherwise false.
   */
  protected function copyFile($from, $to) {
    // @todo Change to file.repository service.
    $file = file_get_contents($from);

    // Load the service statically.
    $filename = \Drupal::service('file_system')->saveData($file, $to, FileExists::Replace);

    return ($filename) ? \Drupal::service('file_url_generator')->generateAbsoluteString($filename) : FALSE;
  }

  /**
   * Function extracts XML from ODT file prepare it and push to convert().
   */
  protected function prepare() {
    // @todo Join prepare() and convert() functions.
    // @todo Replace file_get_contents with XMLReader open() function.
    $xml_string = @file_get_contents('zip://' . $this->odtFileAbsolutePath . '#content.xml');
    if (!$xml_string) {
      $this->messenger()->addError($this->t('Unable to read file contents.'));
    }
    else {
      // Checks that the directory for images exists and is writable.
      if (isset($this->imagesFolder)) {
        \Drupal::service('file_system')->prepareDirectory($this->imagesFolder, FileSystemInterface::CREATE_DIRECTORY);
      }

      // Remove page numbers from TOC.
      $xml_string = preg_replace('/[ \t]*<text:tab\/>\d{1,}/isu', '', $xml_string);

      // Replace invalid links to h1, h2, h3 & etc. anchors.
      // href="#\d{1,}\.([\w\t\d ]){2,}\|outline.
      preg_match_all("/<text:bookmark-start text:name=\"([A-Za-z0-9_-]+)\"\/>(.*?)<text:bookmark-end text:name=\"([A-Za-z0-9_-]+)\"\/>/isu", $xml_string, $raw_bookmarks);

      $bookmarks = [];
      if (!empty($raw_bookmarks) && is_array($raw_bookmarks)) {
        foreach ($raw_bookmarks[2] as $key => $header) {
          $bookmarks[$header . '|outline'] = '#' . $raw_bookmarks[1][$key];
        }
      }

      $xml_string = preg_replace_callback("/#\d{1,}\.(([\w\t\d ]){2,}\|outline)/imu", function ($match) use ($bookmarks) {
        return $bookmarks[$match[1]];
      }, $xml_string);

      // @todo Revise conversion breaks to br's:
      // $xml_string = str_replace('<text:line-break/>', '<br />', $xml_string);
      $this->convert($xml_string);
    }
  }

  /**
   * Function parses XML and converts it to HTML.
   *
   * @param string $xml_string
   *   XML for parsing.
   */
  protected function convert($xml_string) {
    // @todo Move to protected variable?
    $xml = new \XMLReader();

    if (@$xml->xml($xml_string) === FALSE) {
      $this->messenger()->addError($this->t('Invalid file contents.'));

      return FALSE;
    }

    // Now, convert the xml from a string to an HTML.
    $elements_tree = [];

    static $styles = ['Quotations' => ['tags' => ['blockquote']]];

    $translation_table = [];

    // @todo Check if we need to return this here? See below: case 'draw:frame'.
    // $translation_table['draw:frame'] = 'div class="odt-frame"';
    // @todo Implement support for ol li lists.
    if ($this->features['list']) {
      $translation_table['text:list'] = 'ul';
      $translation_table['text:list-item'] = 'li';
    }
    else {
      $translation_table['text:list'] = FALSE;
    }

    if ($this->features['table']) {
      $translation_table['table:table'] = 'table class="table table-condensed table-bordered"';
      $translation_table['table:table-row'] = 'tr';
      $translation_table['table:table-cell'] = 'td';
    }
    else {
      $translation_table['table:table'] = FALSE;
    }

    if ($this->features['toc']) {
      $translation_table['text:table-of-content'] = 'div class="odt-table-of-contents"';
    }
    else {
      $translation_table['text:table-of-content'] = FALSE;
    }

    // We save parent type for extended child processing.
    $parent_type = '';

    while ($xml->read()) {
      // This array will contain the HTML tags opened in every iteration.
      $opened_tags = [];

      if ($xml->nodeType === \XMLReader::END_ELEMENT) {
        // Handle a closing tag.
        if (empty($elements_tree)) {
          continue;
        }

        // Close every opened tag. This should also handle malformed XML files.
        do {
          $element = array_pop($elements_tree);
          if ($element && $element['tags']) {
            // Close opened tags.
            $element['tags'] = array_reverse($element['tags']);
            foreach ($element['tags'] as $html_tag) {
              $html_tag = current(explode(' ', $html_tag));
              $this->html .= '</' . $html_tag . '>';
            }
          }
        } while ($xml->name !== $element['name'] && $element);
        continue;
      }
      elseif (in_array($xml->nodeType, [
        \XMLReader::ELEMENT,
        \XMLReader::TEXT,
        \XMLReader::SIGNIFICANT_WHITESPACE,
      ])) {
        // Handle tags.
        switch ($xml->name) {
          // Text.
          case '#text':
            $this->html .= htmlspecialchars($xml->value);
            break;

          // Title.
          case 'text:h':
            if ($this->features['header']) {
              $n = $xml->getAttribute('text:outline-level');
              if ($n > 6) {
                $n = 6;
              }
              $opened_tags[] = 'h' . $n;
              $bookmark = '';

              // Bookmarks when table of contents exists in odt.
              if ($this->features['bookmark']) {
                $child_xml_string = $xml->readInnerXML();
                if (!empty($child_xml_string)) {
                  if (preg_match('/text:name="([A-Za-z0-9_-]+)"/', $child_xml_string, $bookmark_id)) {
                    $bookmark = ' id="' . $bookmark_id[1] . '"';
                  }
                }
              }

              $this->html .= "\n\n<h$n$bookmark>";
            }
            else {
              $xml->next();
              break;
            }
            break;

          // Paragraph.
          case 'text:p':
            // Just convert odf <text:p> to html <p>.
            if ($xml->isEmptyElement) {
              // Self closed empty <text:p/> tag.
              $this->html .= "\n<p>&nbsp;</p>";
            }
            else {
              $tags = @$styles[$xml->getAttribute('text:style-name')]['tags'];
              if (!(is_array($tags) && in_array('blockquote', $tags))) {
                // Don't print a <p> immediately after or before a <blockquote>.
                $opened_tags[] = 'p';
                $this->html .= "\n<p>";
              }
            }
            break;

          // Link.
          // @todo add options for rel="nofollow" and target="_blank"
          // for external links.
          case 'text:a':
            if ($this->features['link']) {
              $href = $xml->getAttribute('xlink:href');
              $opened_tags[] = 'a';
              $this->html .= '<a href="' . $href . '">';
            }
            else {
              $xml->next();
              break;
            }
            break;

          case 'text:tab':
            $this->html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            break;

          // Frame.
          case 'draw:frame':
            $parent_type = 'paragraph';
            // @todo Consider to remove features['frame'] from config.
            if ($this->features['frame']) {
              $frame_type = $xml->getAttribute('text:anchor-type');
              if ($frame_type == 'as-char') {
                $opened_tags[] = 'span';
                $this->html .= '<span class="odt-frame">';
                $parent_type = 'as-char';
              }
              else {
                $opened_tags[] = 'div';
                $this->html .= '<div class="odt-frame">';
              }
            }
            else {
              $xml->next();
              break;
            }
            break;

          // Image.
          case 'draw:image':
            if ($this->features['image']) {
              $image_file = 'zip://' . $this->odtFileAbsolutePath . '#' . $xml->getAttribute("xlink:href");
              if (isset($this->imagesFolder) && is_dir($this->imagesFolder)) {
                if ($this->isImage($image_file)) {
                  $image_to_save = $this->imagesFolder . '/' . basename($image_file);
                  if (!($src = $this->copyFile($image_file, $image_to_save))) {
                    $this->messenger()->addError($this->t('Unable to move image file.'));
                    break;
                  }
                }
                else {
                  $this->messenger()->addError($this->t('Found invalid image file.'));
                  break;
                }
              }
              else {
                $this->messenger()->addError($this->t('Unable to save the image. Creating a data URL. Image saved directly in the body.'));
                $src = 'data:image;base64,' . base64_encode(file_get_contents($image_file));
              }
              $class = (!empty($parent_type) && $parent_type == 'as-char' ? 'img-inline' : 'img-fluid');
              $this->html .= '<img src="' . $src . '" class="' . $class . '" />';
            }
            else {
              $xml->next();
              $parent_type = '';
              break;
            }
            $parent_type = '';
            break;

          // Line break (<br />).
          case "text:line-break":
            $this->html .= "\n";
            break;

          // Space character.
          case "text:s":
            $count = $xml->getAttribute('text:c');
            if ($count !== NULL) {
              $this->html .= str_repeat(' ', $count);
            }
            else {
              $this->html .= ' ';
            }
            break;

          case "style:style":
            $name = $xml->getAttribute('style:name');
            $parent = $xml->getAttribute('style:parent-style-name');
            if (array_key_exists($parent, $styles)) {
              // Not optimal.
              $styles[$name] = $styles[$parent];
            }

            if ($xml->isEmptyElement) {
              // We can't handle that at the moment.
              break;
            }

            // Read one tag.
            while ($xml->read() && ($xml->name != 'style:style' || $xml->nodeType != \XMLReader::END_ELEMENT)) {
              if ($xml->name == 'style:text-properties') {
                // Creates the style and add <em> to its tags.
                if ($xml->getAttribute('fo:font-style') == 'italic') {
                  $styles[$name]['tags'][] = 'em';
                }

                // Creates the style and add <strong> to its tags.
                if ($xml->getAttribute('fo:font-weight') == 'bold') {
                  $styles[$name]['tags'][] = 'strong';
                }

                // Creates the style and add <b> to its tags.
                if ($xml->getAttribute('style:text-underline-style') == 'solid') {
                  $styles[$name]['tags'][] = 'u';
                }
              }
            }
            break;

          // Note.
          case 'text:note':
            if ($this->features['note']) {
              $note_id = $xml->getAttribute('text:id');
              $note_name = 'Note';

              // Read one tag and stop on </style:style>.
              while ($xml->read() &&  ($xml->name != 'text:note' || $xml->nodeType != \XMLReader::END_ELEMENT)) {
                if ($xml->name == 'text:note-citation' && $xml->nodeType == \XMLReader::ELEMENT) {
                  $note_name = $xml->readString();
                }
                elseif ($xml->name == 'text:note-body' && $xml->nodeType == \XMLReader::ELEMENT) {
                  $note_content = $this->convert($xml->readOuterXML());
                }
              }

              $this->html .= '<sup><a href="#odt-footnote-' . $note_id . '" class="odt-footnote-anchor" name="anchor-odt-' . $note_id . '">$note_name</a></sup>';

              $this->footnotes .= "\n" . '<div class="odt-footnote" id="odt-footnote-' . $note_id . '">';
              $this->footnotes .= '<a class="footnote-name" href="#anchor-odt-' . $note_id . '">' . $note_name . ' .</a> ';
              $this->footnotes .= $note_content;
              $this->footnotes .= '</div>' . "\n";
            }
            else {
              $xml->next();
              break;
            }
            break;

          // Annotation.
          case 'office:annotation':
            if ($this->features['annotation']) {
              $annotation_id = (isset($annotation_id)) ? $annotation_id + 1 : 1;
              $annotation_content = '';
              $annotation_creator = $this->t('Anonymous');
              $annotation_date = '';
              do {
                $xml->read();
                if ($xml->name == 'dc:creator' && $xml->nodeType == \XMLReader::ELEMENT) {
                  $annotation_creator = $xml->readString();
                }
                elseif ($xml->name == 'dc:date' && $xml->nodeType == \XMLReader::ELEMENT) {
                  // @todo Add date format to module configuration.
                  $annotation_date = date("jS \of F Y, H\h i\m", strtotime($xml->readString()));
                }
                elseif ($xml->nodeType == \XMLReader::ELEMENT) {
                  $annotation_content .= $xml->readString();
                  $xml->next();
                }
                // End of the note.
              } while (!($xml->name === 'office:annotation' && $xml->nodeType === \XMLReader::END_ELEMENT));

              $this->html .= '<sup><a href="#odt-annotation-' . $annotation_id . '" name="anchor-odt-annotation-' . $annotation_id . '" title="Annotation (' . $annotation_creator . ')">(' . $annotation_id . ')</a></sup>';
              $this->footnotes .= "\n" . '<div class="odt-annotation" id="odt-annotation-' . $annotation_id . '" >';
              $this->footnotes .= '<a class="annotation-name" href="#anchor-odt-annotation-' . $annotation_id . '"> (' . $annotation_id . ')&nbsp;</a>';
              $this->footnotes .= "\n" . '<b>' . $annotation_creator . ' (<i>' . $annotation_date . '</i>)</b> :';
              $this->footnotes .= "\n" . '<div class="odt-annotation-content">' . $annotation_content . '</div>';
              $this->footnotes .= '</div>' . "\n";
            }
            else {
              $xml->next();
              break;
            }
            break;

          default:
            if (array_key_exists($xml->name, $translation_table)) {
              if ($translation_table[$xml->name] === FALSE) {
                $xml->next();
                break;
              }

              // $tag[0] is the tag name, other indexes are attributes.
              $tag = explode(' ', $translation_table[$xml->name], 1);
              $attributes = '';

              $opened_tags[] = $tag[0];

              // Support colspan and rowspan attributes for tables.
              if ($xml->name == 'table:table-cell') {
                if ($xml->getAttribute('table:number-columns-spanned') !== NULL) {
                  $attributes .= ' colspan = "' . $xml->getAttribute('table:number-columns-spanned') . '"';
                }
                elseif ($xml->getAttribute('table:number-rows-spanned') !== NULL) {
                  $attributes .= ' rowspan = "' . $xml->getAttribute('table:number-rows-spanned') . '"';
                }
              }

              $this->html .= "\n<" . $translation_table[$xml->name] . $attributes . '>';
            }
        }
      }

      // Opening tag.
      if ($xml->nodeType === \XMLReader::ELEMENT && !($xml->isEmptyElement)) {
        $current_element_style = $xml->getAttribute('text:style-name');
        if ($current_element_style && isset($styles[$current_element_style])) {
          // Styling tags management.
          foreach ($styles[$current_element_style]['tags'] as $html_tag) {
            $this->html .= '<' . $html_tag . '>';
            $opened_tags[] = $html_tag;
          }
        }
        $elements_tree[] = [
          'name' => $xml->name,
          'tags' => $opened_tags,
        ];
      }
    }
  }

}
