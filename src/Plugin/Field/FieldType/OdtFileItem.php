<?php

namespace Drupal\odt_importer\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'odt_file' field type.
 *
 * @FieldType(
 *   id = "odt_file",
 *   label = @Translation(".odt file importer"),
 *   description = @Translation("This field stores the ID of an .odt file which would be converted to html and stored in attached long text field."),
 *   category = @Translation("Reference"),
 *   default_widget = "file_generic",
 *   default_formatter = "file_default",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class OdtFileItem extends FileItem {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'file_extensions' => 'odt',
      'destination_field' => '',
      'text_format' => '',
    ] + parent::defaultFieldSettings();
    unset($settings['description_field']);
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'target_id' => [
          'description' => 'The ID of the file entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'display' => [
          'description' => 'Flag to control whether this file should be displayed when viewing content.',
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'default' => 1,
        ],
      ],
      'indexes' => [
        'target_id' => ['target_id'],
      ],
      'foreign keys' => [
        'target_id' => [
          'table' => 'file_managed',
          'columns' => ['target_id' => 'fid'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    unset($properties['description']);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    // @todo check that this lib attached from parent or uncomment line:
    // $element['#attached']['library'][] = 'file/drupal.file';
    $this->getFieldDefinition()->getFieldStorageDefinition()->getSettings();

    $scheme_options = \Drupal::service('stream_wrapper_manager')->getNames(StreamWrapperInterface::WRITE_VISIBLE);
    $element['uri_scheme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Upload destination'),
      '#options' => $scheme_options,
      '#default_value' => $this->getSetting('uri_scheme'),
      '#description' => $this->t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    // Get base form from FileItem.
    $element = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Remove the description option.
    unset($element['description_field']);

    // Get all text fields for current entity type and bundle.
    $destination_field_options = [];

    $entity = $form_state->getFormObject()->getEntity();
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity->get('entity_type'), $entity->get('bundle'));
    foreach ($fields as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), [
        'text',
        'text_long',
        'text_with_summary',
      ])) {
        $destination_field_options[$field_name] = $field_definition->getLabel();
      }
    }

    $element['destination_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination field'),
      '#description' => $this->t('Select the name of text field which will be used to store converted html code.'),
      '#options' => $destination_field_options,
      '#default_value' => $settings['destination_field'] ?? '',
      '#required' => TRUE,
      '#weight' => 11,
    ];

    // Get all enabled text formats.
    $text_format_options = [];
    $filter_formats = filter_formats();
    if (!empty($filter_formats)) {
      foreach ($filter_formats as $filter_name => $filter_format) {
        $text_format_options[$filter_name] = $filter_format->label();
      }
    }

    $element['text_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Default text format'),
      '#description' => $this->t('Select default text format for converted html code.'),
      '#options' => $text_format_options,
      '#default_value' => $settings['text_format'] ?? '',
      '#required' => TRUE,
      '#weight' => 11,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $settings = $field_definition->getSettings();

    // Prepare destination.
    $dirname = static::doGetUploadLocation($settings);
    \Drupal::service('file_system')->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY);

    // Generate a file entity.
    $destination = $dirname . '/' . $random->name(10, TRUE) . '.odt';
    $data = $random->paragraphs(3);
    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = \Drupal::service('file.repository');
    $file = $file_repository->writeData($data, $destination, FileExists::Error);
    $values = [
      'target_id' => $file->id(),
      'display' => (int) $settings['display_default'],

      // @todo add destination field? Example:
      // 'description' => $random->sentences(10),
    ];
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isDisplayed() {
    // OdtFile items do not have per-item visibility settings.
    return FALSE;
  }

}
